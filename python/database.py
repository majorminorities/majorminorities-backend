from flask_sqlalchemy import SQLAlchemy
import traceback
import datetime
import dateutil

db = SQLAlchemy()

P_NUM_FIELDS = 15
M_NUM_FIELDS = 17
I_NUM_FIELDS = 12

P_DATA_PATH = 'data/people.txt'
M_DATA_PATH = 'data/minority.txt'
I_DATA_PATH = 'data/industry.txt'


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(30), unique=True, nullable=False)
    birth_date = db.Column(db.Date)
    death_date = db.Column(db.Date)
    gender = db.Column(db.String(15))
    birth_place = db.Column(db.String(30))
    minority_1 = db.Column(db.String(30))
    minority_2 = db.Column(db.String(30), nullable=True)
    occupation = db.Column(db.String(50))
    industry = db.Column(db.String(100))
    twitter_handle = db.Column(db.String(20))
    youtube = db.Column(db.String(200))
    website = db.Column(db.String(200))
    image = db.Column(db.String(200), nullable=True)
    external_link = db.Column(db.String(200))
    bio = db.Column(db.String(1000))

    age = db.Column(db.Integer)
    death_date = db.Column(db.Date, nullable=True)

    def __init__(self, name, birth_date, death_date, gender, birth_place, minority_1, minority_2, occupation, industry, twitter_handle, youtube, website, image, external_link, bio):
        self.name = name
        self.birth_date = birth_date
        self.death_date = death_date
        self.gender = gender
        self.birth_place = birth_place
        self.minority_1 = minority_1
        self.minority_2 = minority_2
        self.occupation = occupation
        self.industry = industry
        self.twitter_handle = twitter_handle
        self.youtube = youtube
        self.website = website
        self.image = image
        self.external_link = external_link
        self.bio = bio

        # calculate age
        if death_date != 'None':
            self.age = (dateutil.parser.parse(death_date) -
                        dateutil.parser.parse(birth_date)).days/365
        else:
            self.age = (datetime.datetime.today() -
                        dateutil.parser.parse(birth_date)).days/365

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'birth_date': self.birth_date,
            'death_date': self.death_date,
            'gender': self.gender,
            'birth_place': self.birth_place,
            'minority_1': self.minority_1,
            'minority_2': self.minority_2,
            'occupation': self.occupation,
            'industry': self.industry,
            'image': self.image,
            'twitter_handle': self.twitter_handle,
            'youtube': self.youtube,
            'website': self.website,
            'bio': self.bio,
            'age': self.age,
            'external_link': self.external_link

        }


class Minority(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(30))
    percentage_us = db.Column(db.Float)
    origin_countries = db.Column(db.String(100))
    rep_arts = db.Column(db.Float)
    rep_politics = db.Column(db.Float)
    rep_music = db.Column(db.Float)
    rep_med = db.Column(db.Float)
    rep_edu = db.Column(db.Float)
    rep_comm = db.Column(db.Float)
    rep_bus = db.Column(db.Float)
    rep_food = db.Column(db.Float)
    rep_sci = db.Column(db.Float)
    rep_eng = db.Column(db.Float)
    rep_csci = db.Column(db.Float)
    image = db.Column(db.String(200))
    external_link = db.Column(db.String(200))
    media = db.Column(db.String(200))

    def __init__(self, name, percentage_us, origin_countries, rep_arts, rep_politics, rep_music, rep_med, rep_edu, rep_comm, rep_bus, rep_food, rep_sci, rep_eng, rep_csci, image, external_link, media):
        self.name = name
        self.percentage_us = percentage_us
        self.origin_countries = origin_countries
        self.rep_arts = rep_arts
        self.rep_politics = rep_politics
        self.rep_music = rep_music
        self.rep_med = rep_med
        self.rep_edu = rep_edu
        self.rep_comm = rep_comm
        self.rep_bus = rep_bus
        self.rep_food = rep_food
        self.rep_sci = rep_sci
        self.rep_eng = rep_eng
        self.rep_csci = rep_csci
        self.image = image
        self.external_link = external_link
        self.media = media

    def serialize(self):
        return {
            'name': self.name,
            'percentage_us': self.percentage_us,
            'origin_countries': self.origin_countries,
            'percentages': {'Arts and Entertainment': self.rep_arts, 'Politics': self.rep_politics, 'Music': self.rep_music, 'Medicine': self.rep_med, 'Education': self.rep_edu, 'Community and Social Service': self.rep_comm, 'Business': self.rep_bus, 'Food': self.rep_food, 'Life, Physical, and Social Science': self.rep_sci, 'Architecture and Engineering': self.rep_eng, 'Computer Science and Mathematics': self.rep_csci},
            'image': self.image,
            'external_link': self.external_link,
            'media': self.media
        }


class Industry(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100))
    avg_salary = db.Column(db.Float)
    rep_women = db.Column(db.Float)
    rep_latinx = db.Column(db.Float)
    rep_afr = db.Column(db.Float)
    rep_aa = db.Column(db.Float)
    rep_lgbtq = db.Column(db.Float)
    rep_na = db.Column(db.Float)
    top_fields = db.Column(db.String(200))
    div_score = db.Column(db.Float)
    image = db.Column(db.String(200))
    external_link = db.Column(db.String(200))
    media = db.Column(db.String(200))

    def __init__(self, name, avg_salary, rep_women, rep_latinx, rep_afr, rep_aa, rep_lgbtq, rep_na, top_fields, image, external_link, media):
        self.name = name
        self.avg_salary = avg_salary
        self.rep_women = rep_women
        self.rep_latinx = rep_latinx
        self.rep_afr = rep_afr
        self.rep_aa = rep_aa
        self.rep_lgbtq = rep_lgbtq
        self.rep_na = rep_na
        self.top_fields = top_fields
        self.div_score = (float(rep_women) + float(rep_latinx) +
                          float(rep_afr) + float(rep_aa)) / 4
        self.image = image
        self.external_link = external_link
        self.media = media

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'avg_salary': self.avg_salary,
            'percentages': {'Women': self.rep_women, 'Lantinx': self.rep_latinx, 'African American': self.rep_afr, 'Asian American': self.rep_aa, 'LGBTQ': self.rep_lgbtq, 'Native American': self.rep_na},
            'top_fields': self.top_fields,
            'div_score': self.div_score,
            'image': self.image,
            'external_link': self.external_link,
            'media': self.media
        }


class Variables(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    spotlight_date = db.Column(db.Date)
    spotlight_id = db.Column(db.Integer)

    def __init__(self, spotlight_date="0000-00-00", spotlight_id=0):
        self.spotlight_date = spotlight_date
        self.spotlight_id = spotlight_id

    def get_spotlight_date(self):
        return self.spotlight_date

    def get_spotlight_id(self):
        return self.spotlight_id


def database_is_empty():
    table_names = sa.inspect(engine).get_table_names()
    return table_names == []


def fill_people():
    if Person.query.all() != []:
        print("person table exists!")
        return
    print("Adding people to database...")
    model_dict = {"name": None, "birth_date": None, "death_date": None, "gender": None, "minority_1": None, "minority_2": None,
                  "birth_place": None, "occupation": None, "industry": None, "twitter_handle": None, "youtube": None, "website": None, "image": None, "external_link": None, "bio": None}
    person_dict = None

    f = open(P_DATA_PATH, "r")
    contents = f.readlines()
    it = iter(contents)
    try:
        count = 1
        person_dict = {key: "" for key in model_dict}
        dict_iter = iter(model_dict)
        for line in contents:
            line = line.strip()

            if count < P_NUM_FIELDS:
                person_dict[next(dict_iter)] = line
                count += 1
                continue

            if len(line) > 0:
                person_dict['bio'] += line
                continue
            else:  # but we need to do this at eof too
                count = 1
                dict_iter = iter(model_dict)
                # print(person_dict['name'])
                db.session.add(Person(**person_dict))
                # print(person_dict['name'])
                person_dict = {key: "" for key in model_dict}
        # Send the very last one
        db.session.add(Person(**person_dict))
        db.session.commit()
    except Exception as ex:
        traceback.print_exc()
    f.close()


def fill_minorities():
    if Minority.query.all() != []:
        print("minority table exists!")
        return
    print("Adding minorities to database...")
    model_dict = {"name": None, "percentage_us": None, "origin_countries": None, "rep_arts": None,
                  "rep_politics": None, "rep_music": None, "rep_med": None, "rep_bus": None, "rep_food": None, "rep_sci": None,
                  "rep_eng": None, "rep_csci": None, "rep_edu": None, "rep_comm": None, "image": None, "external_link": None, "media": None}
    minority_dict = None

    f = open(M_DATA_PATH, "r")
    contents = f.readlines()
    it = iter(contents)
    try:
        count = 1
        minority_dict = {key: "" for key in model_dict}
        dict_iter = iter(model_dict)
        for line in contents:
            line = line.strip()

            if count <= M_NUM_FIELDS:
                minority_dict[next(dict_iter)] = line
                count += 1
                continue

            if len(line) == 0:
                count = 1
                dict_iter = iter(model_dict)
                # print(minority_dict['name'])
                db.session.add(Minority(**minority_dict))
                minority_dict = {key: "" for key in model_dict}
        # Send the very last one
        db.session.add(Minority(**minority_dict))
        db.session.commit()
    except Exception as ex:
        traceback.print_exc()
    f.close()


def fill_industries():
    if Industry.query.all() != []:
        print("industry table exists!")
        return
    print("Adding industries to database...")
    model_dict = {"name": None, "avg_salary": None, "rep_women": None, "rep_latinx": None, "rep_afr": None,
                  "rep_aa": None, "rep_lgbtq": None, "rep_na": None, "top_fields": None, 'image': None, 'external_link': None, 'media': None}
    industry_dict = None

    f = open(I_DATA_PATH, "r")
    contents = f.readlines()
    it = iter(contents)
    try:
        count = 1
        industry_dict = {key: "" for key in model_dict}
        dict_iter = iter(model_dict)
        for line in contents:
            line = line.strip()

            if count <= I_NUM_FIELDS:
                industry_dict[next(dict_iter)] = line
                count += 1
                continue

            if len(line) == 0:
                count = 1
                dict_iter = iter(model_dict)
                # print(minority_dict['name'])
                db.session.add(Industry(**industry_dict))
                industry_dict = {key: "" for key in model_dict}
        # Send the very last one
        db.session.add(Industry(**industry_dict))
        db.session.commit()
    except Exception as ex:
        traceback.print_exc()
    f.close()


def fill_variables():
    if Variables.query.all() != []:
        print("variable table exists!")
        return
    db.session.add(Variables(spotlight_date="1111-11-11"))
    db.session.commit()


def fill_db():
    fill_people()
    fill_minorities()
    fill_industries()
    fill_variables()
