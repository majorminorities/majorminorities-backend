import requests
import json
import flask
import traceback

PEOPLE_IP = 'http://192.168.99.100:5000/api/person'
JSON_HEADER = {'content-type': 'application/json'}


def test():
    model_dict = {"name": "Anika", "birth_date": "1997-06-26", "minority_1": "Women", "minority_2": None,
                  "birth_place": "Fairfax", "occupation": "Student", "image": "None", "bio": "Anika Gera makes a living making Jordan sad"}
    requests.post('http://192.168.99.100:5000/api/person',
                  data=json.dumps(model_dict), headers={'content-type': 'application/json'})


def fill_people():
    model_dict = {"name": None, "birth_date": None, "minority_1": None, "minority_2": None,
                  "birth_place": None, "occupation": None, "image": None, "bio": None}
    person_dict = None

    f = open("people.txt", "r")
    contents = f.readlines()
    it = iter(contents)
    try:
        count = 1
        person_dict = {key: "" for key in model_dict}
        dict_iter = iter(model_dict)
        for line in contents:
            line = line.strip()

            if count < 8:
                person_dict[next(dict_iter)] = line
                count += 1
                continue

            if len(line) > 0:
                person_dict['bio'] += line
                continue
            else:  # but we need to do this at eof too
                count = 1
                dict_iter = iter(model_dict)
                print(person_dict['name'])
                person_dict['id'] = 0
                requests.post(PEOPLE_IP, data=json.dumps(
                    person_dict), headers=JSON_HEADER)
                person_dict = {key: "" for key in model_dict}
        # Send the very last one
        requests.post(PEOPLE_IP, data=json.dumps(
            person_dict), headers=JSON_HEADER)
    except Exception as ex:
        traceback.print_exc()


if __name__ == "__main__":
    fill_people()
    # test()
    print("Done")
