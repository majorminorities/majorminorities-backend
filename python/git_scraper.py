"""
Script that updates data from GitLab
Jordan Taylor - 10/22/2018
"""

import requests

USERS = {'Jordan Taylor': None, 'Mark Ramirez': None, 'Anika Gera': None,
         'Anika': None, 'Sheetal Poduri': None, 'Jennie Kim': None, 'jk1324': None}
KEYS = [8981174, 8981119]


class GitData:
    name = ''
    commits = 0
    issues = 0
    tests = 0

    def __init__(self, name, commits=0, issues=0, tests=0):
        self.name = name
        self.commits = commits
        self.issues = issues
        self.tests = tests

    def __str__(self):
        return "Commits: " + str(self.commits) + ", Issues: " + str(self.issues) + ", Unit Tests: " + str(self.tests)

    def serialize(self):
        return {'name': self.name, 'commits': self.commits, 'issues': self.issues, 'tests': self.tests}


def reset():
    USERS['Jordan Taylor'] = GitData("Jordan Taylor", 0, 0, 3)
    USERS['Mark Ramirez'] = GitData("Mark Ramirez", 0, 0, 3)
    USERS['Anika Gera'] = GitData("Anika Gera", 0, 0, 3)
    USERS['Jennie Kim'] = GitData("Jennie Kim", 0, 0, 3)
    USERS['Sheetal Poduri'] = GitData("Sheetal Poduri", 0, 0, 4)
    USERS['Anika'] = USERS['Anika Gera']
    USERS['jk1324'] = USERS['Jennie Kim']


def get_commits():
    for key in KEYS:
        page = 1
        empty = False
        while not empty:
            response = requests.get(url="https://gitlab.com/api/v4/projects/" + str(
                key) + "/repository/commits?all=true&page=" + str(page))
            json = response.json()
            empty = len(json) == 0
            for commit in json:
                if commit["author_name"] in USERS:
                    USERS[commit["author_name"]].commits += 1
                    # print(commit["author_name"])
            page += 1


def get_issues():
    for key in KEYS:
        page = 1
        empty = False
        while not empty:
            response = requests.get(
                url="https://gitlab.com/api/v4/projects/" + str(key) + "/issues?all=true&page=" + str(page))
            json = response.json()
            empty = len(json) == 0
            for issue in json:
                if issue['state'] == 'closed':
                    for assignee in issue['assignees']:
                        if assignee["name"] in USERS:
                            USERS[assignee["name"]].issues += 1
                            # print(commit["author_name"])
            page += 1


def get_tests():
    pass


def update_all_git():
    reset()
    get_commits()
    get_issues()
    get_tests()


def get_git_data():
    return [USERS[user].serialize() for user in USERS]
