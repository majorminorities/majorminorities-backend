import requests

WIKIDATA_URL = "https://www.wikidata.org/w/api.php?"
WIKIPEDIA_URL = "https://en.wikipedia.org/w/api.php?"


def translate(my_list):
    result = []
    for val in my_list:
        response = requests.get(url=WIKIDATA_URL, params={
                                "action": "wbgetentities", "props": "labels", "ids": val, "format": "json", "languages": "en"})
        json = response.json()
        result.append(json["entities"][val]["labels"]["en"]["value"])
    return result


def find_people(names):
    response = requests.get(url=WIKIPEDIA_URL, params={
                            "action": "query", "prop": "pageprops", "titles": names.replace(" ", "_"), "format": "json"})
    json = response.json()
    result = []
    # print(json)
    for val in json["query"]["pages"]:
        result.append(json["query"]["pages"][val]
                      ["pageprops"]["wikibase_item"])
    return result

# given a q# return the asked for data


def scrape():
    print("We're scraping!")

    people_unformatted = input(
        "Who do you want to get data on?: ").replace(",", "|")
    names = people_unformatted.split('|')
    people = find_people(people_unformatted)
    # get name P1559
    # get birthday P569
    # get place of birth P19
    # get minority(s) P172
    # get occupation P106
    # get industry
    # get summary wikipedia
    f = open("data/people.txt", "a")
    for i, person in enumerate(people):
        person_data = []
        # get the summary of this person
        title = names[i].replace(" ", "_")
        response = requests.get(url=WIKIPEDIA_URL, params={
                                "action": "query", "prop": "extracts", "exintro": 0, "explaintext": 0, "redirects": "1", "titles": title, "format": "json"})
        json = response.json()

        for val in json["query"]["pages"]:
            print(json["query"]["pages"][val]["extract"])
            person_data.append(json["query"]["pages"][val]["extract"])

        # get the picture of this person
        response = requests.get(url=WIKIPEDIA_URL, params={
                                "action": "query", "prop": "pageimages", "titles": title, "piprop": "original", "format": "json"})
        json = response.json()
        # print(json)
        try:
            for val in json["query"]["pages"]:
                print(json["query"]["pages"][val]["original"]["source"])
                person_data.append(json["query"]["pages"]
                                   [val]["original"]["source"])
        except:
            person_data.append("<No Picture>")
        # get the rest of the data
        response = requests.get(url=WIKIDATA_URL, params={
                                'action': 'wbgetclaims', 'entity': person, 'format': 'json', 'redirects': '1'})
        json = response.json()
        result_list = []
        # print(data)
        # print(data["claims"])
        # All of the data that might not exist for a person will throw an exception if we index for it
        try:
            result_list.append(
                json["claims"]["P106"][0]["mainsnak"]["datavalue"]["value"]["id"])  # occupation
        except KeyError:
            pass
        try:
            result_list.append(json["claims"]["P19"][0]["mainsnak"]
                               ["datavalue"]["value"]["id"])  # place of birth
        except KeyError:
            pass
        try:
            # minority (ethnicity)
            result_list.append(json["claims"]["P172"][0]
                               ["mainsnak"]["datavalue"]["value"]["id"])
        except KeyError:
            pass

        person_data.extend(translate(result_list))
        person_data.append(json["claims"]["P569"][0]["mainsnak"]
                           ["datavalue"]["value"]["time"][1:11])  # birthday
        person_data.append(names[i])  # name

        for data_point in reversed(person_data):
            f.write(data_point + "\n")
    f.write("\n")
    f.close()


if __name__ == "__main__":
    scrape()
