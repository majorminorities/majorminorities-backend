from io import StringIO
from flask import Flask
import unittest as ut
from flask_testing import TestCase
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc as sa_exc
# from myapp import create_app, db

import traceback
import datetime
import dateutil
import dateutil.parser
import warnings

from database import db, Person, Minority, Industry, Variables, fill_db, fill_people, fill_minorities, fill_industries

NUM_P = 49
NUM_M = 3
NUM_I = 6


class TestDB(TestCase):

    def create_app(self):

        app = Flask(__name__)
        app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://majorminorities:AJJMSswe2018@majorminoritiest-test-db.cvrts2uqr0m2.us-east-2.rds.amazonaws.com/majorminoritiestest"
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        app.config['TESTING'] = True
        db.app = app
        db.init_app(app)
        db.create_all()
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.commit()
        db.drop_all()

    def test_person(self):
        # self, name, birth_date, death_date, gender, birth_place, minority_1, minority_2, occupation, industry, twitter_handle, youtube, website, image, external_link, bio)
        person = Person(name='Anika', birth_date='1997-06-26', death_date='3000-06-26', gender='female', birth_place='fairfax',
                        minority_1='women', minority_2='none', occupation='student', industry='technology', twitter_handle='@imanika', youtube='www.youtube.com/imanika', website='none', image='none', bio='none', external_link='none')
        db.session.add(person)
        db.session.commit()
        assert person in Person.query.all()

    def test_fill_people(self):
        fill_people()
        assert len(Person.query.all()) >= NUM_P

    def test_minority(self):

        minority = Minority(name='fake_name', percentage_us="20.0", origin_countries="All of them", rep_arts="5.0",
                            rep_politics="5.0", rep_music="5.0", rep_med="5.0", rep_bus="5.0", rep_food="5.0", rep_sci="5.0",
                            rep_eng="5.0", rep_csci="5.0", rep_edu="5.0", rep_comm="5.0", image="www.googgle.com", external_link="www.wikipedia.com", media="www.ask.com")
        db.session.add(minority)
        db.session.commit()
        assert minority in Minority.query.all()

    def test_fill_minorities(self):
        fill_minorities()
        assert len(Minority.query.all()) >= NUM_M

    def test_industry(self):

        industry = Industry(name="fake_name", avg_salary="999999", rep_women="1.0", rep_latinx="1.0",
                            rep_afr="1.0", rep_aa="1.0", rep_lgbtq="1.0", rep_na="1.0", top_fields="fake_fields", image="none", external_link="none", media="none")
        db.session.add(industry)
        db.session.commit()
        assert industry in Industry.query.all()

    def test_fill_industries(self):
        fill_industries()
        assert len(Industry.query.all()) >= NUM_I


if __name__ == '__main__':
    ut.main()
