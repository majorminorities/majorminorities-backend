
from flask import Flask, abort, redirect
from flask import json as fjson
from flask_sqlalchemy import SQLAlchemy
from flask import request as f_request
from flask_cors import CORS
import sqlalchemy as sa
import flask_restless
import datetime
import dateutil
import git_scraper
import random
from database import db, Person, Minority, Industry, Variables, fill_db


app = Flask(__name__)
CORS(app)
# real db 'mysql://majorminorities:AJJMSswe2018@majorminoritiesdb.cvrts2uqr0m2.us-east-2.rds.amazonaws.com/majorminorities'
# testdb  "mysql://majorminorities:AJJMSswe2018@majorminoritiest-test-db.cvrts2uqr0m2.us-east-2.rds.amazonaws.com/majorminoritiestest"
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://majorminorities:AJJMSswe2018@majorminoritiesdb.cvrts2uqr0m2.us-east-2.rds.amazonaws.com/majorminorities'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.app = app
db.init_app(app)
engine = sa.create_engine(
    'mysql://majorminorities:AJJMSswe2018@majorminoritiesdb.cvrts2uqr0m2.us-east-2.rds.amazonaws.com/majorminorities', convert_unicode=True)

"""
This is the REST API
"""


@app.route("/")
def hello():
    return redirect("https://documenter.getpostman.com/view/5451325/RWgjahhS")


@app.route("/person")
@app.route("/person/<id>")
def person(id=0):
    my_query = Person.query
    if(id != 0):
        my_query = my_query.filter(Person.id == id)

    return fjson.jsonify([person.serialize() for person in my_query.all()])


@app.route("/minority")
@app.route("/minority/<id>")
def minority(id=0):
    my_query = Minority.query
    if(id != 0):
        my_query = my_query.filter(Minority.id == id)

    return fjson.jsonify([minority.serialize() for minority in my_query.all()])


@app.route("/industry")
@app.route("/industry/<id>")
def industry(id=0):
    my_query = Industry.query
    if(id != 0):
        my_query = my_query.filter(Industry.id == id)

    return fjson.jsonify([industry.serialize() for industry in my_query.all()])


@app.route('/subset')
def subset():
    # here are all the possible parameters
    #variables = ['age2', 'age', 'birth_place', 'gender', 'occupation', 'minority', 'industry']
    my_query = Person.query
    if 'age2' in f_request.args:
        my_query = my_query.filter(f_request.args['age'] <= Person.age).filter(
            Person.age <= f_request.args['age2'])
    elif 'age' in f_request.args:
        my_query = my_query.filter(f_request.args['age'] == Person.age)
    if 'gender' in f_request.args:
        my_query = my_query.filter(f_request.args['gender'] == Person.gender)
    if 'birth_place' in f_request.args:
        my_query = my_query.filter(
            f_request.args['birth_place'] == Person.birth_place)
    if 'occupation' in f_request.args:
        my_query = my_query.filter(
            f_request.args['occupation'] == Person.occupation)
    if 'minority' in f_request.args:
        my_query = my_query.filter(sa.or_(
            f_request.args['minority'] == Person.minority_1, f_request.args['minority'] == Person.minority_2))
    if 'industry' in f_request.args:
        my_query = my_query.filter(
            f_request.args['industry'] == Person.industry)
    return fjson.jsonify([person.serialize() for person in my_query.all()])


@app.route('/score')
@app.route('/score/<name>')
def score(name=""):
    # you got it anika
    my_query = Industry.query.with_entities(Industry.name, Industry.div_score)
    if len(name) > 0:
        my_query = my_query.filter(Industry.name == name)
    industries = {industry[0]: industry[1] for industry in my_query.all()}
    return fjson.jsonify(industries)


@app.route('/spotlight')
def spotlight():
    interval = datetime.timedelta(days=7)
    # woo, go anika - i am leaving this here because it is encouraging
    current_date = datetime.datetime.today()
    my_var_query = Variables.query.first()
    old_date = dateutil.parser.parse(str(my_var_query.get_spotlight_date()))
    person_query = Person.query
    if (current_date - old_date > interval):
        new_person_id = random.randint(1, 49)
        person_query = person_query.filter(Person.id == new_person_id)
        my_var_query.spotlight_id = new_person_id
        my_var_query.spotlight_date = current_date
        db.session.commit()
    else:
        person_query = person_query.filter(
            Person.id == my_var_query.get_spotlight_id())

    return fjson.jsonify([person.serialize() for person in person_query.all()])


@app.route('/search')
def search():
    # given search terms, return elements that contain the search terms
    # takes optional parameters to limit the search to industry, person, or minority
    person = 'person' in f_request.args
    industry = 'minority' in f_request.args
    minority = 'minority' in f_request.args

    # if no limits given, search all
    if not (person or industry or minority):
        person, industry, minority = True, True, True

    if 'terms' in f_request.args:
        terms = "%" + str(f_request.args['terms']) + "%"
    else:
        return abort(400)

    result = []

    if person:
        base_query = Person.query
        base_query = base_query.filter(sa.or_(
            Person.name.like(terms),
            Person.birth_date.like(terms),
            Person.death_date.like(terms),
            Person.gender.like(terms),
            Person.minority_1.like(terms),
            Person.minority_2.like(terms),
            Person.occupation.like(terms),
            Person.industry.like(terms),
            Person.bio.like(terms)))
        result.extend([person.serialize() for person in base_query.all()])

    if minority:
        base_query = Minority.query
        base_query = base_query.filter(sa.or_(
            Minority.name.like(terms),
            Minority.percentage_us.like(terms),
            Minority.origin_countries.like(terms),
            Minority.rep_arts.like(terms),
            Minority.rep_politics.like(terms),
            Minority.rep_music.like(terms),
            Minority.rep_med.like(terms),
            Minority.rep_edu.like(terms),
            Minority.rep_comm.like(terms),
            Minority.rep_bus.like(terms),
            Minority.rep_food.like(terms),
            Minority.rep_sci.like(terms),
            Minority.rep_eng.like(terms),
            Minority.rep_csci.like(terms)))
        result.extend([minority.serialize() for minority in base_query.all()])

    if industry:
        base_query = Industry.query
        base_query = base_query.filter(sa.or_(
            Industry.name.like(terms),
            Industry.avg_salary.like(terms),
            Industry.rep_women.like(terms),
            Industry.rep_latinx.like(terms),
            Industry.rep_afr.like(terms),
            Industry.rep_aa.like(terms),
            Industry.top_fields.like(terms),
            Industry.div_score.like(terms)))
        result.extend([industry.serialize() for industry in base_query.all()])

    return fjson.jsonify(result)


@app.route('/git')
def get_git():
    git_scraper.update_all_git()
    return fjson.jsonify(git_scraper.get_git_data())


if __name__ == "__main__":
    db.create_all()
    fill_db()
    manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
    print("Running")
    app.run(host="0.0.0.0")
