FROM python:3

MAINTAINER Jordan Taylor "jordanfjtaylor@gmail.com"

RUN apt-get update -y && \
    apt-get -y install vim

RUN pip install --upgrade pip && \
    pip install flask && \
    pip install flask-restless && \
    pip install flask-sqlalchemy && \
    pip install flask-cors && \
    pip install Flask-Testing && \
    pip install mysqlclient && \
    pip install requests

COPY . /app

WORKDIR /app

EXPOSE 5000

ENTRYPOINT ["python"]

CMD ["python/main.py"]
