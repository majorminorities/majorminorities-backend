def search(q):
    try:
        from googlesearch import search
    except ImportError:
        print("No module named 'google' found")

# to search
    query = q

    for j in search(query, tld="co.in", num=1, stop=1, pause=2):
        print(j)

if __name__ == "__main__":
    search("Morgan Freeman Famous Acting Youtube Link");
    search("Oprah Famous Speaking Youtube Link");
    # search("Gina Rodriguez acting youtube link");
    # search("Elena Kagan Famous Youtube Link");
    # search("Sonia Sotomayor Famous Youtube Link");
    # search("Ruth Bader Ginsburg Famous Youtube Link");
    # search("Martin Luther King Jr. Famous Speech Youtube Link");
    # search("Barack Obama Famous Speech Youtube Link");
    # search("Michelle Obama Famous Speech Youtube Link");
    # search("Anita Borg Famous Speech Youtube Link");
    # search("Indra Nooyi Famous Speech Youtube Link");
    # search("Marc Benioff Famous Speech Youtube Link");
    # search("Ben Carson Famous Youtube Link");
    # search("Viola Davis Famous Acting Youtube Link");
    # search("Denzel Washington Famous Acting Youtube Link");
    # search("Constance Wu Famous Acting Youtube Link");
    # search("Youtube video about Patricia Bath ophthalmologist");
    # search("Youtube video about Daniel Hale Williams cardiologist");
    # search("Youtube video about Joycelyn Elders pediatrician");
    # search("Shellye Archambeau youtube video");
    # search("Kimberly Bryant youtube video Black Girls Code");
    # search("youtube video about simmie knox paintings");
    # search("Stevie Wonder Famous Singing Youtube Link");
    # search("Aretha Franklin Famous Singing Youtube Link");
    # search("Shakira Famous Singing Youtube Link");
    # search("Selena Famous Singing Youtube Link");
    # search("Carlos Santana Famous Singing Youtube Link");
    # search("youtube video about Kat Von D tatoos");
    # search("youtube video about Frida Kahlo art");
    # search("youtube video about Salvador Dali Art");
    # search("youtube video about Severo Ochoa doctor");
    # search("youtube video about Helen Rodriguez Trias");
    # search("Marcel Claure Keynote Speech Youtube Link");
    # search("L. Rafael Reif Keynote Speech Youtube Link");
    # search("Grace Hopper Famous Speech Youtube Link");
    # search("Sandra Day O'Connor Famous Speech Youtube Link");
    # search("Wendy Davis Famous Speech Youtube Link");
    # search("Jennifer Lopez Famous Performance Youtube Link");
    # search("Cecile Richards Planned Parenthood Famous Speech Youtube Link");
    # search("youtube video about Elizabeth Blackwell");
    # search("youtube video about Rosalyn Sussman Yalow");
    # search("Beyonce Famous Performance Youtube Link");
    # search("youtube video about Annie Leibowitz Photography");
    # search("youtube video about Kate Spade");
    # search("Frank Ocean Famous Performance Youtube Link");
    # search("Sam Smith Famous Performance Youtube Link");
    # search("Sia Famous Performance Youtube Link");
    # search("Jon Hall Linux Keynote Speech Youtube Link");
    # search("Tim Cook Keynote Speech Youtube Link");
